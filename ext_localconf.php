<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

defined('TYPO3_MODE') || die();

// Make the extension configuration accessible
$extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
);
$bootstrapPackageConfiguration = $extensionConfiguration->get('bootstrap_package');
$ebpConfiguration = $extensionConfiguration->get('extended_bootstrap_package');

// PageTS
// Add Content Elements
if (!$bootstrapPackageConfiguration['disablePageTsContentElements']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:extended_bootstrap_package/Configuration/TsConfig/Page/ContentElement/All.tsconfig">');
}
if (!(bool) $bootstrapPackageConfiguration['disablePageTsBackendLayouts']) {
    // Disable bootstrap_package backend layouts
    if ((bool) $ebpConfiguration['enableBootstrapPackageBackendLayouts']) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            "@import 'EXT:extended_bootstrap_package/Configuration/TsConfig/Page/Mod/WebLayout/DisableBackendLayouts.tsconfig'"
        );
    }
    // Theme backend layouts
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        "@import 'EXT:extended_bootstrap_package/Configuration/TsConfig/Page/Mod/WebLayout/BackendLayouts/*.tsconfig'"
    );
}
// Disable bootstrap_package container elements
if ((bool) $ebpConfiguration['enableBootstrapPackageContainerElements']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        "@import 'EXT:extended_bootstrap_package/Configuration/TsConfig/Page/Mod/RemoveBPContainer.tsconfig'"
    );
}
// TCEFORM
if (!$bootstrapPackageConfiguration['disablePageTsTCEFORM']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:extended_bootstrap_package/Configuration/TsConfig/Page/TCEFORM.tsconfig">');
}
// PageTS
$versionInformation = GeneralUtility::makeInstance(Typo3Version::class);
// Only include page.tsconfig if TYPO3 version is below 12 so that it is not imported twice.
if ($versionInformation->getMajorVersion() < 12) {
    ExtensionManagementUtility::addPageTSConfig(
        '@import "EXT:t3_theme_base/Configuration/page.tsconfig"'
    );
}
// Add default RTE configuration for extended bootstrap package
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['bootstrap'] = 'EXT:extended_bootstrap_package/Configuration/RTE/Default.yaml';

// Register Icons
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

$icons = [
    'countdown',
    'counter-group',
    'counter-group-item',
    'progress-bar-group',
    'progress-bar-group-item',
    'pricing-table',
    'pricing-plan',
    'testimonials',
    'testimonial-item',
    'team-list',
    'team-member',
    'cta-buttons',
    'cta-buttons-item',
    'features',
    'features-item',
    'animated-header',
    'sticky-content',
];
foreach ($icons as $icon) {
    $iconRegistry->registerIcon(
        'content-extendedbootstrappackage-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:extended_bootstrap_package/Resources/Public/Icons/ContentElements/' . $icon . '.svg']
    );
}

foreach (['frame', 'no-frame'] as $icon) {
    $iconRegistry->registerIcon(
        'ebp-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => sprintf('EXT:extended_bootstrap_package/Resources/Public/Icons/PageLayout/%s.svg', $icon)]
    );
}
// Add backend preview hook
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['extended_bootstrap_package'] =
     T3graf\ExtendedBootstrapPackage\Hooks\PageLayoutViewDrawItem::class;

// Register "ebp" as global fluid namespace

$GLOBALS['TYPO3_CONF_VARS']['SYS']['fluid']['namespaces']['ebp'][] = 'T3graf\\ExtendedBootstrapPackage\\ViewHelpers';
