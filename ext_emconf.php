<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'Extended Bootstrap Package',
    'description' => 'Extends Benjamin Kott\'s Bootstrap package with new, useful functions such as mega menus, an optional top bar panel, new CE\'s and more. Furthermore, it\'s a good example how you can adapt the bootstrap package to your own needs.',
    'category' => 'templates',
    'author' => 'Development-Team',
    'author_email' => 'development@t3graf-media.de',
    'author_company' => 'T3graf media-agentur UG',
    'state' => 'beta',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '3.1.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'bootstrap_package' => '14.0.0-14.9.99',
            'container' => '2.0.0-2.9.99',
            'content_defender' => '3.4.0-3.4.99',
            'iconpack' => '0.3.0-0.3.99',
            'bootstrap_package_iconpack' => '0.1.0-0.1.99',
            'iconpack_fontawesome' => '0.2.0-0.2.99',
            'iconpack_bootstrap' => '1.1.0-1.1.99',

        ],
        'conflicts' => [],
        'suggests' => [
            'mindshape_cookie_consent' => '*'
        ],
    ],
];
