<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\ExtendedBootstrapPackage\Hooks;

use TYPO3\CMS\Backend\Form\Exception;
use TYPO3\CMS\Backend\Form\FormDataCompiler;
use TYPO3\CMS\Backend\Form\FormDataGroup\TcaDatabaseRecord;
use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

class PageLayoutViewDrawItem implements PageLayoutViewDrawItemHookInterface
{
    /**
     * @var array
     */
    // 'CType' => 'Filename of BackendView html'
    protected $supportedContentTypes = [
        'animated_header' => 'AnimatedHeader',
        'counter_group' => 'CounterGroup',
        'progressbar_group' => 'ProgressbarGroup',
        'countdown' => 'Countdown',
        'pricing_table' => 'PricingTable',
        'testimonials' => 'Testimonials',
        'team_members' => 'TeamMembers',
        'cta_buttons' => 'CtaButtons',
        'feature_list' => 'FeatureList',
    ];

    /**
     * @var StandaloneView
     */
    protected $view;

    public function __construct(StandaloneView $view = null)
    {
        $this->view = $view ?? GeneralUtility::makeInstance(StandaloneView::class);
    }

    /**
     * Preprocesses the preview rendering of a content element.
     *
     * @param PageLayoutView $parentObject
     * @param bool $drawItem
     * @param string $headerContent
     * @param string $itemContent
     * @param array $row
     */
    public function preProcess(PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row): void
    {
        if (!isset($this->supportedContentTypes[$row['CType']])) {
            return;
        }
        $formDataGroup = GeneralUtility::makeInstance(TcaDatabaseRecord::class);
        $formDataCompiler = GeneralUtility::makeInstance(FormDataCompiler::class, $formDataGroup);
        $formDataCompilerInput = [
            'command' => 'edit',
            'tableName' => 'tt_content',
            'vanillaUid' => (int)$row['uid'],
        ];

        try {
            $flexform = '';
            $result = $formDataCompiler->compile($formDataCompilerInput);
            $processedRow = $this->getProcessedData($result['databaseRow'], $result['processedTca']['columns']);
            // Sammelt die Flexform-Einstellungen und entfernt bestimmte Array-Keys ("data", "sDEF", "lDEF", "vDEF") zur besseren Nutzung in Fluid
            if ($row['pi_flexform']) {
                $flexform = $this->cleanUpArray(GeneralUtility::xml2array($row['pi_flexform']), ['data', 'sDEF', 'lDEF', 'vDEF']);
            }

            $this->configureView($result['pageTsConfig'], $row['CType']);
            $this->view->assignMultiple(
                [
                    'row' => $row,
                    'processedRow' => $processedRow,
                    'pi_flexform_transformed' => $flexform,
                ]
            );

            $itemContent = $parentObject->linkEditContent($this->view->render(), $row);
        } catch (Exception $exception) {
            $message = $GLOBALS['BE_USER']->errorMsg;
            if (empty($message)) {
                $message = $exception->getMessage() . ' ' . $exception->getCode();
            }

            $itemContent = $message;
        }

        $drawItem = false;
    }

    /**
     * @param array $pageTsConfig
     * @param string $contentType
     */
    protected function configureView(array $pageTsConfig, $contentType): void
    {
        if (empty($pageTsConfig['mod.']['web_layout.']['tt_content.']['preview.'])) {
            return;
        }

        $previewConfiguration = $pageTsConfig['mod.']['web_layout.']['tt_content.']['preview.'];
        $extensionKey = 'extended_bootstrap_package.';
        if (!empty($previewConfiguration[$extensionKey]['templateRootPath'])) {
            $this->view->setTemplateRootPaths([
                'EXT:extended_bootstrap_package/Resources/Private/Backend/Templates/ContentElements/',
                $previewConfiguration[$extensionKey]['templateRootPath'],
            ]);
        }
        if (!empty($previewConfiguration[$extensionKey]['layoutRootPath'])) {
            $this->view->setLayoutRootPaths([
                $previewConfiguration[$extensionKey]['layoutRootPath'],
            ]);
        }
        if (!empty($previewConfiguration[$extensionKey]['partialRootPath'])) {
            $this->view->setPartialRootPaths([
                $previewConfiguration[$extensionKey]['partialRootPath'],
            ]);
        }
        $this->view->setTemplate($this->supportedContentTypes[$contentType]);
    }

    /**
     * @param array $databaseRow
     * @param array $processedTcaColumns
     * @return array
     */
    protected function getProcessedData(array $databaseRow, array $processedTcaColumns)
    {
        $processedRow = $databaseRow;
        foreach ($processedTcaColumns as $field => $config) {
            if (!isset($config['children'])) {
                continue;
            }
            $processedRow[$field] = [];
            foreach ($config['children'] as $child) {
                if (!$child['isInlineChildExpanded']) {
                    $formDataGroup = GeneralUtility::makeInstance(TcaDatabaseRecord::class);
                    $formDataCompiler = GeneralUtility::makeInstance(FormDataCompiler::class, $formDataGroup);
                    $formDataCompilerInput = [
                        'command' => 'edit',
                        'tableName' => $child['tableName'],
                        'vanillaUid' => $child['vanillaUid'],
                    ];
                    $child = $formDataCompiler->compile($formDataCompilerInput);
                }
                $processedRow[$field][] = $this->getProcessedData($child['databaseRow'], $child['processedTca']['columns']);
            }
        }

        return $processedRow;
    }

    /**
     * @param array $cleanUpArray
     * @param array $notAllowed
     * @return array|mixed
     */
    protected function cleanUpArray(array $cleanUpArray, array $notAllowed)
    {
        $cleanArray = [];
        foreach ($cleanUpArray as $key => $value) {
            if (in_array($key, $notAllowed, true)) {
                return is_array($value) ? $this->cleanUpArray($value, $notAllowed) : $value;
            }
            if (is_array($value)) {
                $cleanArray[$key] = $this->cleanUpArray($value, $notAllowed);
            }
        }

        return $cleanArray;
    }
}
