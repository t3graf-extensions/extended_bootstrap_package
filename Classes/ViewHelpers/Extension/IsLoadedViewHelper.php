<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\ExtendedBootstrapPackage\ViewHelpers\Extension;

/*
 * This file is part of the T3graf\ExtendedBootstrapPackage project under GPLv2 or later.
 * he is forked from FluidTYPO3/Vhs project
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

/**
 * ### Extension: Loaded (Condition) ViewHelper
 *
 * Condition to check if an extension is loaded.
 *
 * ### Example:
 *
 * ```
 * {v:extension.isLoaded(extensionName: 'news', then: 'yes', else: 'no')}
 * ```
 *
 * ```
 * <v:extension.isLoaded extensionName="news">
 *     ...
 * </v:extension.isLoaded>
 * ```
 */
class IsLoadedViewHelper extends AbstractConditionViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument(
            'extensionName',
            'string',
            'Name of extension that must be loaded in order to evaluate as TRUE, UpperCamelCase',
            true
        );
    }

    /**
     * This method decides if the condition is TRUE or FALSE. It can be overriden in extending viewhelpers
     * to adjust functionality.
     *
     * @param array $arguments ViewHelper arguments to evaluate the condition for this ViewHelper, allows for
     *                         flexiblity in overriding this method.
     * @return bool
     */
    protected static function evaluateCondition($arguments = null): bool
    {
        $extensionName = $arguments['extensionName'];
        $extensionKey = GeneralUtility::camelCaseToLowerCaseUnderscored($extensionName);
        $isLoaded = ExtensionManagementUtility::isLoaded($extensionKey);
        return true === $isLoaded;
    }
}
