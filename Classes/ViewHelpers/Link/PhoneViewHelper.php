<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\ExtendedBootstrapPackage\ViewHelpers\Link;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * Phone link ViewHelper.
 * Generates an Phone link.
 *
 * Examples
 * ========
 *
 * Basic phone link
 * ----------------
 *
 * ::
 *
 *    <f:link.phone phone="1234-546" />
 *
 * Output::
 *
 *    <a href="tel:1234546">1234-546</a>
 *
 *
 * Phone link with custom linktext
 * -------------------------------
 *
 * ::
 *
 *    <f:link.phone phone="1234-546">Call us</f:link.email>
 *
 * Output::
 *
 *    <a href="tel:1234546">Call us</a>
 */
class PhoneViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'a';

    /**
     * Arguments initialization.
     */
    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument('phone', 'string', 'The phone number to be turned into a link', true);
        $this->registerUniversalTagAttributes();
        $this->registerTagAttribute('name', 'string', 'Specifies the name of an anchor');
        $this->registerTagAttribute('rel', 'string', 'Specifies the relationship between the current document and the linked document');
        $this->registerTagAttribute('rev', 'string', 'Specifies the relationship between the linked document and the current document');
        $this->registerTagAttribute('target', 'string', 'Specifies where to open the linked document');
    }

    /**
     * @return string Rendered phone link
     */
    public function render()
    {
        $phone = $this->arguments['phone'];
        //$pattern = '!(\b\+?[0-9()\[\]./ -]{7,17}\b|\b\+?[0-9()\[\]./ -]{7,17}\s+(extension|x|#|-|code|ext)\s+[0-9]{1,6})!i';
        //echo preg_replace('/\D/', '', $phone);
        //print_r($phonenumber);
        //die();

        $linkHref = 'tel:' . $phone;
        $linkText = htmlspecialchars($phone);
        $escapeSpecialCharacters = true;

        $tagContent = $this->renderChildren();
        if ($tagContent !== null) {
            $linkText = $tagContent;
        }
        $this->tag->setContent($linkText);
        $this->tag->addAttribute('href', $linkHref, $escapeSpecialCharacters);
        $this->tag->forceClosingTag(true);

        return $this->tag->render();
    }
}
