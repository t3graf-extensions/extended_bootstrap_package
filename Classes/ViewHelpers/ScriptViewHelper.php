<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\ExtendedBootstrapPackage\ViewHelpers;

/*
 *  Copyright notice
 *
 *  (c) 2013 Marc Hirdes <Marc_Hirdes@gmx.de>, clickstorm GmbH
 *  (c) 2013 Mathias Brodala <mbrodala@pagemachine.de>, PAGEmachine AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Renders a HTML-script value by moving it into a temporary file and adding it to the page.
 *
 * == Examples ==
 *
 * <code title="Default parameters">
 * <vh:script>'foo <b>bar</b>.'</js:script>
 * </code>
 * <output>
 * <script type="text/javascript">
 *        foo <b>bar<\/b>
 * </script>
 *
 * <code title="Inline notation">
 * {someText -> vh:script}
 * </code>
 * <output>
 * <script type="text/javascript">
 *  someText
 * </script>
 * </output>
 */
class ScriptViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
    public function render(): void
    {
        GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class)
            ->addJsFooterFile(
                GeneralUtility::writeJavaScriptContentToTemporaryFile($this->renderChildren())
            );
    }
}
