<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\ExtendedBootstrapPackage\ViewHelpers\Condition\String;

/*
 * This file is part of the T3graf\ExtendedBootstrapPackage project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

/**
 * ### Condition: String contains substring.
 *
 * Condition ViewHelper which renders the `then` child if provided
 * string $string contains provided string $phrase.
 */
class ContainsViewHelper extends AbstractConditionViewHelper
{
    /**
     * Initialize arguments.
     */
    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument('string', 'string', 'string', true);
        $this->registerArgument('phrase', 'string', 'phrase', true);
    }

    /**
     * @param array $arguments
     * @return bool
     */
    protected static function evaluateCondition($arguments = null)
    {
        return false !== strpos($arguments['string'], $arguments['phrase']);
    }
}
