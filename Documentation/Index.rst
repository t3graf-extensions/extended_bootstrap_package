﻿.. include:: Includes.txt

.. _start:

=============================================================
Extended Bootstrap Package
=============================================================

:Classification:
    extended_bootstrap_package

:Version:
    |release|

:Language:
    en

:Description:
      Extends Benjamin Kott's **Bootstrap package** with new, useful functions such as mega menus, an optional top bar panel, new CE's and more. Furthermore, it's a good example how you can adapt the bootstrap package to your own needs.

:Keywords:
      frontend, Bootstrap Package, theme,	responsive,	bootstrap

:Copyright:
    2020-2021

:Author:
    Development-Team (T3graf media-agentur UG)

:Email:
    development@t3graf-media.de

:License:
      Open Content License available from `www.opencontent.org/opl.shtml
      <http://www.opencontent.org/opl.shtml>`_

:Rendered:
      |today|

The content of this document is related to TYPO3, a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

**Table of Contents**

.. toctree::
   :maxdepth: 3
   :titlesonly:

   Introduction/Index
   EditorManual/Index
   AdministratorManual/Index
   DeveloperManual/Index
   Tutorials/Index
   Sitemap
