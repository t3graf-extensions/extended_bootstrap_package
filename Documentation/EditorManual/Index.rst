﻿.. include:: ../Includes.txt


.. _editorManual:

================
For editors
================

This chapter describes how to use the extension from a user point of view.


.. toctree::
   :maxdepth: 3
   :titlesonly:

   NewFeatures/Index
   ContentElements/Index

