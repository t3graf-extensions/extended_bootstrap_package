﻿.. include:: Includes.txt

.. _externalTutorial:

External tutorials
-------------------

There are already some additional 3rd party tutorials about the extension "extended_bootstrap_package".

Video tutorials by T3grafmedia
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

5 german video tutorials by Mike Tölle which can be found at http://t3graf-media.de/blog/video-anleitungen/typo3-extensions/news.html.

- Installation & Configuration: http://vimeo.com/63231058
- Create an article: http://vimeo.com/63231385
- Output in frontend: http://vimeo.com/63231754
- News archive: http://vimeo.com/63232250
- Multilanguage: http://vimeo.com/63232527


Add links to your own tutorials
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you have written an own tutorial or found one which is worth to be mentioned,
please open an issue in the bugtracker.
