.. include:: /Includes.txt

.. _about:

What does it do?
================

It extends bootstrap_package by Benjamin Kott and serves as the basis for creating websites or distributions using the Bootstrap framework.

Features
--------

* 2-col, 3-col, 4-col more flexible grid container with EXT:container

  * flexible background-color or image,
  * variable width for each column

* Extra class fields for headline and subheadline
* Top info bar
* Navbar (header) full-width
* Footer full-width
* Footer slide out effect
* Fontawesome 5.14.0 in ckeditor
* Large dropdown menus
* Mega menus
* New cookie consent with EXT: mindshape_cookie_consent,
  preconfigured for youtube, vimeo
* New Backend Layout for maintenance page

  * with optional backgroundimage slider

* implements the functionality of the EXT:textimage_size
  thanks to Alessandro & Giulia Tuveri for the code
* New CE's

  * Counter group
  * Progress Bar group
  * Countdown
  * Pricing Table
  * Testimonials
  * Team List
  * CTA Buttons
  * Feature List

Credits
=======

This extension was created by Mike Tölle in 2020 for T3graf media-agentur UG, Recklinghausen.

This extension can be provided thanks to the excellent work of TYPO3 community members. A big thank you goes to:

- Benjamin Kott, extension `bootstrap_package <https://extensions.typo3.org/extension/bootstrap_package>`__
- B13 GmbH, extension `container <https://extensions.typo3.org/extension/container>`__
- Alessandro & Giulia Tuveri, extension `textimage_size <https://extensions.typo3.org/extension/textimage_size>`__
