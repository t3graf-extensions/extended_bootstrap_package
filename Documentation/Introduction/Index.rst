﻿.. include:: /Includes.txt

.. _introduction:

============
Introduction
============

Why we have create this extension?
==================================

We use the bootstrap package with corresponding site-package as standard for all of our customer projects. Since we didn't always want to copy all changes and new features into the site-package, we have packed all important changes and new features into this extension.

.. toctree::
   :maxdepth: 5
   :titlesonly:

   About/Index
   Screenshots/Index
