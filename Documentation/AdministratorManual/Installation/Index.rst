﻿.. include:: ../Includes.txt



.. _installation:

============
Installation
============

The extension needs to be installed as any other extension of TYPO3 CMS:

#. Switch to the module :guilabel:`Admin Tools > Extensions`.

#. Get the extension

   #. **Use composer**: Use `composer require t3graf/extended-bootstrap-package`.

   #. **Get it from the Extension Manager:** Press the :guilabel:`Retrieve/Update`
      button and search for the extension key *extended_bootstrap_package* and import the
      extension from the repository.

   #. **Get it from typo3.org:** You can always get current version from
      `https://extensions.typo3.org/extension/extended_bootstrap_package/
      <https://extensions.typo3.org/extension/extended_bootstrap_package/>`__ by
      downloading either the t3x or zip version. Upload
      the file afterwards in the Extension Manager.

Latest version from git
-----------------------
You can get the latest version from git by using the git command:

.. code-block:: bash

   git clone https://gitlab.com/t3graf-extensions/extended_bootstrap_package.git

Preparation: Include static TypoScript
--------------------------------------

.. warning::

   You don't have to insert the TypoScript code from the EXT: bootstrap package separately.

   The Extended Bootstrap Package takes care of that.

   If you have already included the TypoScript code from the EXT: bootstrap package, remove it.

The extension ships some TypoScript code which needs to be included.

#. Switch to the root page of your site.

#. Switch to :guilabel:`Template > Info/Modify`.

#. Press the link :guilabel:`Edit the whole template record` and switch to the tab :guilabel:`Includes`.

#. Select :guilabel:`Extended Bootstrap Package: Bootstrap 5.x (extended_bootstrap_package)` at the field :guilabel:`Include static (from extensions):`

.. important::

   If you are using bootstrap v4 you must include the :guilabel:`Extended Bootstrap Package: Bootstrap 4.x (extended_bootstrap_package)`

.. figure:: ../../Images/AdministratorManual/include_ts.png
   :class: with-shadow
   :alt: Introduction Package
   :width: 600px
