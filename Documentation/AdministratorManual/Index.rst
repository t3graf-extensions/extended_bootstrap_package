﻿.. include:: /Includes.txt


.. _administrator:

==================
For administrators
==================

This chapter describes how to manage the extension from a superuser point of view.


.. toctree::
   :maxdepth: 5
   :titlesonly:

   Installation/Index
   Configuration/Index

