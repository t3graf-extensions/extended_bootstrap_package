(function () {
  function getTitleElement(collapsible) {
    return collapsible.closest('.ebp-page-ce').querySelector('.ebpc-title');
  }
  const pageCeCollapsibles = document.querySelectorAll('.ebp-page-ce .ebpc-collapse');
  pageCeCollapsibles.forEach((el) => {
    el.addEventListener('show.bs.collapse', (e) => getTitleElement(e.target).classList.add('d-none'));
    el.addEventListener('hide.bs.collapse', (e) => getTitleElement(e.target).classList.remove('d-none'));
  })
})();

