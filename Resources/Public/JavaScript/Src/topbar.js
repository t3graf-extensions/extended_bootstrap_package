function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}
ready(() => {
  if (window.matchMedia('(max-width: 991px)').matches) {
    document.querySelector('#top-bar').classList.remove('show');
  }

  if (window.matchMedia('(min-width: 992px)').matches) {
    document.querySelector('#top-bar').classList.add('show');

  }
});

window.addEventListener('resize', function(event) {
  if (window.matchMedia('(max-width: 991px)').matches) {
    document.querySelector('#top-bar').classList.remove('show');
  }
  if (window.matchMedia('(min-width: 992px)').matches) {
    document.querySelector('#top-bar').classList.add('show');
  }
}, true);
// When the user scrolls the page, execute navStickyFunction
['scroll', 'resize', 'DOMContentLoaded'].forEach(function(e) {
  window.addEventListener(e, navStickyFunction);
  window.addEventListener(e, topbarShow);

});
document.getElementById("top-bar").addEventListener("transitionend", topbarShow);
// Get the navbar
var navbar = document.getElementById("page-header");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;
//var position = navbar.offsetHeight;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function topbarShow () {

  if (window.matchMedia('screen and (max-width: 990px)').matches) {
    document.getElementById("top-bar").style.top = window.pageYOffset+"px";
    if (window.pageYOffset >= sticky) {
      document.getElementById("page-header").style.top = "unset";
    }
  }
  if (window.matchMedia('screen and (min-width: 992px)').matches) {
    document.getElementById("top-bar").style.top = "0";
    document.getElementById("page-header").style.top = "0";
  }
}
function navStickyFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("navbar-fixed-top");
    navbar.classList.add("navbar-transition");
  } else {
    navbar.classList.remove("navbar-fixed-top");
    navbar.classList.remove("navbar-transition");
  }

}
