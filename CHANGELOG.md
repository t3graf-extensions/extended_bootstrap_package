# 3.1.0

## FEATURE

- 00038c1 [FEATURE] add EXT:iconpack for icons
- bc5eeff [FEATURE] add flexible container/section ce
- e76d235 [FEATURE] add content animations
- 7c87614 [FEATURE] make navbar colors customizable
- f669975 [FEATURE] make footer switchable
- 8a16e7e [FEATURE] add some ne backend layouts
- 67be246 [FEATURE] add Sticky Content element
- 4e3052b [FEATURE] add CE Animated Header
- c872775 [FEATURE] add Tabs to use for content elements

## TASK

- 40f5452 [TASK] update TYPO3 version
- 88c5021 [TASK] update README.md
- f65caa4 [TASK] add mega menu function
- 4326d0b [TASK] add mega menu function
- e12d3ca [TASK] update EPB to BP v14 & adjust to iconpack
- e59d399 [TASK] update PHP version to 8.1
- 7f8eebf [TASK] drop PHP version 7.4
- 62a8445 [TASK] update extension depends
- 6c72235 [TASK] change bootstrap_package version to 14.0
- 7943864 [TASK] add styles to gt-tabs
- 956998c [TASK] make structure container more flexible
- f6c6cc2 [TASK] make gt-tabs more flexible
- a28818f [TASK] run cgl
- 8cd6b11 [TASK] remove own Google Analytics js code
- 10ddebe [TASK] hide TCA table for animated header
- f42014a [TASK] add phplint for 8.0 and 8.1
- 29f5dee [TASK] run cgl test
- 23459f8 [TASK] convert jquery to vanilla js
- 3ad2909 [TASK] exclude fontawesome.yaml
- 720efe7 [TASK] run CGL check
- 6cdfbb4 [TASK] update rules for php-cs-fixer
- 0c52a97 [TASK] make make countdown more functional
- 8341082 [TASK] drop support for bootstrap 4
- 99ef321 [TASK] make TYPO3 compatible
- 6381d62 [TASK] update Fontawesome
- 385cfef [TASK] convert jquery to vanilla js
- eb27cd4 [TASK] make TYPO3 v12 compatible
- e7bebf0 [TASK] convert jquery to vanilla js
- 95b1a5a [TASK] convert jquery to vanilla js

## BUGFIX

- 0e88a03 [BUGFIX] fix problems with slide out footer with BP v14
- fce9585 [BUGFIX] change default layout to container layout
- 630ecf9 [BUGFIX] change some TCA of page
- d96b495 [BUGFIX] fix background-color problems by containers
- 74e6936 [BUGFIX] fix width by empty container cols
- 2f50e9f [BUGFIX] fix error with scss variables
- 3211d70 [BUGFIX] fix problem with element nodes

## MISC

- 84d7525 [Feature] add CE accordion to page structure

## Contributors

- Mike Tölle

# 3.0.0

## BREAKING

- 26681ea [!!!][TASK] change page structure columns in EXT:container

## FEATURE

- fc0f4bd [FEATURE] add tile container
- 491d40b [FEATURE] add more backend usability.

## TASK

- 286578f [TASK] update .gitlab-ci.yml
- 469e6aa [TASK] run CGL
- 26681ea [!!!][TASK] change page structure columns in EXT:container
- 53710b5 [TASK] add T3editor
- da894c7 [TASK] insert environment variables in .ddev/config.yaml
- 5a2b6ca [TASK] change fontawesome path.
- a954138 [TASK] raise versions TYPO3 11.5 PHP 8.0
- 08e5e55 [TASK] update .gitlab-ci.yml

## BUGFIX

- b098f6d [BUGFIX] fix backend usability for EXT:container
- 0e13d8d [BUGFIX] fix collapsed error in backend usability.
- b44b78a [BUGFIX] fix array to string convert error in Backend
- 7ef5314 [BUGFIX] .gitlab-ci.yml
- d2c5388 [BUGFIX] .gitlab-ci.yml
- 0141873 [BUGFIX] .gitlab-ci.yml

## Contributors

- Mike Tölle

# 2.2.0

## TASK

- 3557c82 [TASK] remove support for maps2
- 970bc1b [TASK] make cookie consent switchable between bootstrap_package and mindshape_cookie_consent.

## BUGFIX

- 6295a7d [BUGFIX] fix warning about array key.

## Contributors

- Mike Tölle

# 2.1.6

## TASK

- a94f162 [TASK] update requirements.

## Contributors

- Mike Tölle

# 2.1.5

## FEATURE

- b5676e2 [FEATURE] allow html tags in header

## BUGFIX

- a8e6b69 [BUGFIX] center pricing table in row

## Contributors

- Mike Tölle

# 2.1.4

## TASK

- d2e103b [TASK] make EBP-Grids more flexible

## BUGFIX

- 73bb2b3 [BUGFIX] fix problem with ddev and PHPSTORM

## Contributors

- Mike Tölle

# 2.1.3

## TASK

- 6ec8cbe [TASK] change state to beta
- c959a37 [TASK] update some dev-requirements

## BUGFIX

- 8d50ff0 [BUGFIX] fix problem with composer replace.self

## Contributors

- Mike Tölle

# 2.1.2

## BUGFIX

- 57169b1 [BUGFIX] fix show grid background-images without content

## Contributors

- Mike Tölle

# 2.1.1

## TASK

- f9fc35c [TASK] add phpstorm+ddev+xdebug support
- 437842e [TASK] prepare for documentation creation and rendering

## Contributors

- Mike Tölle

# 2.1.0

## BREAKING

- 31a40fc [!!!][BREAKING] Remove 1-col from EBP-Grid (no longer necessary). This function is included in the bootstrap package.

## FEATURE

- 45528a6 [FEATURE] add background-image function to EBP-Grids

## TASK

- 1ef47ec [TASK] make some changes
- 05f241f [TASK] make top-info-bar more comfortable

## BUGFIX

- f3a2de0 [BUGFIX] change title for language menu constant in top-info-bar
- 341494e [BUGFIX] fix problem with width in EBP columns-grids
- 6e0a06c [BUGFIX] fix problem with topbar collapse

## Contributors

- Mike Tölle

# 2.0.10

## TASK

- 20f22b8 [TASK] change backend layout images

## BUGFIX

- bef5ba9 [BUGFIX] fix path to backend layout html file

## Contributors

- Mike Tölle

# 2.0.9

## FEATURE

- 0569931 [FEATURE] implement the functionality from EXT:textimage_size
- ec01fc3 [FEATURE] add new backend layout for maintain page

## TASK

- c0bde33 [TASK] update some extensions
- 0d97a15 [TASK] update mindshape/mindshape-cookie-consent

## BUGFIX

- 2905844 [BUGFIX] fix some issues for maintain page

## Contributors

- Mike Tölle

# 2.0.8

## TASK

- 7c043c4 [TASK] add google tag manager support
- 590c737 [TASK] change some css settings for large-dropdown
- 5509460 [TASK] delete Google Analytics script. mindshape/seo insert it itself

## Contributors

- Mike Tölle

# 2.0.7

## BUGFIX

- b898ea7 [BUGFIX] fix problem with sticky footer and breadcrumbs

## Contributors

- Mike Tölle

# 2.0.6

## BUGFIX

- 6c45d59 [BUGFIX] fix problem top info header in small devices
- 6922a26 [BUGFIX] fix problem with large dropdown width

## Contributors

- Mike Tölle

# 2.0.5

## TASK

- 333dcd0 [TASK] make colors for topinfobar variable
- b7bf23a [TASK] set col_xs defaults in Flexform
- 05615f4 [TASK] change position of cookie button for videos
- cd8663f [TASK] change title for static templates
- 34993d0 [TASK] clean up code.
- f2cce50 [TASK] change TypoScript includes to @import

## BUGFIX

- a724ddc [BUGFIX] fix breakpoint problems with header
- 7c269d0 [BUGFIX] allow  tag in  tag.

## Contributors

- Mike Tölle

# 2.0.4

## BUGFIX

- 4fb9020 [BUGFIX] fix responsible problems in top bar info

## Contributors

- Mike Tölle

# 2.0.3

## TASK

- 3f7bceb [TASK] update composer
- 86a2182 [TASK] make grids more confortable
- 9daac5b [TASK] make topinfobar more responible

## BUGFIX

- a407c6c [BUGFIX] revert composer version
- ce784bd [BUGFIX] fix typo error
- ba2000a [BUGFIX] add new CE's to the selectorlist

## Contributors

- Mike Tölle

# 2.0.2

## TASK

- 9596ec0 [TASK] change colors for large-headline

## BUGFIX

- 7ebb0ca [BUGFIX] fix topinfobar destroys menubar
- 9f64250 [BUGFIX] fix toggle button for no topinfobar
- 08779a2 [BUGFIX] fix javascript loading for topinfobar

## Contributors

- Mike Tölle

# 2.0.1

# 2.0.0

## BREAKING

- 8c15648 [!!!][TASK] upgrade to bootstrap package 12.0

## TASK

- 8c15648 [!!!][TASK] upgrade to bootstrap package 12.0
- dc7d60e [TASK] update php-cs-fixer and install phpstan.

## BUGFIX

- 359c636 [BUGFIX] fix header block.

## Contributors

- Mike Tölle

# 1.1.0

## TASK

- f914058 [TASK] add cookie consent for google-analytics.
- e103a91 [TASK] add mindshape cookie consent.

## BUGFIX

- 39d9ce6 [BUGFIX] fix problem with scss directory .
- e17b51f [BUGFIX] fix problem with links in slide out footer.
- 3e80f66 [BUGFIX] fix problem with optional border section in page layout.
- bf5699c [BUGFIX] fix problem with Fontawesome path
- 09fe3f9 [BUGFIX] fix problem with none enabled Top Bar (outsource js in own file)
- 526ee8e [BUGFIX] double css files in themes

## MISC

- 98b22b2 [DOCS] begin documenting

## Contributors

- Mike Tölle

# 1.0.0

## FEATURE

- d8fd265 [FEATURE] add Footer slide out effect
- a4fe525 [FEATURE] add new CE Feature List
- d864cfd [FEATURE] add new CE CTA Buttons
- dede80d [FEATURE] make mainnavigation (header) full-width.
- 722bb90 [FEATURE] add new CE Team List
- ed3d85e [FEATURE] add new CE Testimonials
- 3b3be76 [FEATURE] add new CE Pricing Table
- 5259033 [FEATURE] add new CE Countdown
- 0e7f569 [FEATURE] add new CE Progress bar
- 0fbf5d1 [FEATURE] add new CE Countergroup
- 52d6b58 [FEATURE] Add mega menus
- 53b9225 [FEATURE] Add large dropdown menus
- 40af241 [FEATURE] Add fontawesome 5.14.0 and implement it to ckeditor.
- dcf4f7f [FEATURE] Add top info bar
- 8e15ae4 [FEATURE] Add extra class fields for headline and subheadline
- 9f4d3fd [FEATURE] Add  2-col, 3-col, 4-col grid container with EXT:container

## TASK

- 335346a [TASK] make Header Top Panel full-width if Navbar is full-width.
- 1aaed62 [TASK] outsource CE js with ViewHelper asset.script
- 116c99d [TASK] add lazy loading to image tags
- ed8f32f [TASK] add divider color to CE CTA Buttons.
- 113fd2a [TASK] add 1 column CE to Grids as a wrapper.
- 13454c6 [TASK] add group EBP Elements to select list
- d2b66a5 [TASK] make preview more user friendly.
- 55ff5ca [TASK] more usability in CE's
- 564cc9e [TASK] add more informations to grids in preview
- b9c66a4 [TASK] add different flexform tab title for every CE
- e0c290e [TASK] exclude files for documentation rendering command
- 953bc4f [TASK] adapt to bootstrap package 11.0.3
- 323e206 [TASK] adjust the imports to bootstrap_package BUGFIX: Resolve SCSS files correctly when using symlinked paths
- 863f8c9 [TASK] change description text
- 624aac6 [TASK] Fix some problems
- 350786d [TASK] add CE preview in backend
- 13ed739 [TASK] add size selectable to counter groups
- 04d9f37 [TASK] add flexform to CE countdown
- 9e4283d [TASK] add support for TYPO3 v11
- a969f19 [TASK] add fontawesome icons to top info bar
- a28da3a [TASK] add extension key to composer.json
- 8cf436d [TASK] add new CI/CD badge to README.md
- f832629 [TASK] make extended_bootstrap_package available in sys_template
- ff9e5f4 [TASK] add .gitlab-ci.yml and required files

## BUGFIX

- 9370603 [BUGFIX] ix progressbar circle js
- cebbc7a [BUGFIX] fix layout for Pricing Table
- 2e6ece1 [BUGFIX] fix new CE array
- 562a3e9 [BUGFIX] After updating to bootstrap_package 11.0.3 we need to add transition to the navigation bar
- bec485d [BUGFIX] make icon height and width available
- 859f935 [BUGFIX] change directory path

## MISC

- c07c250 TASK] Drop TYPO3 Version 9
- 437eab6 TASK] Add PHP 7.4 to workflow
- 5ced8d6 push to master
- 82f67d8 add License.md
- 1f31ce2 Merge remote-tracking branch 'extended_bootstrap_package/master' into master
- 79e35b9 Gelöscht .build/config/sites/autogenerated-1-c4ca4238a0/config.yaml
- 26d2a0d Initial commit

## Contributors

- Mike Tölle
- T3graf

