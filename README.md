# TYPO3 Extension: `Extended Bootstrap Package`
[![TYPO3 compatibility](https://img.shields.io/static/v1?label=TYPO3&message=v11%20%7C%20v12&color=f49800&&logo=typo3&logoColor=f49800)](https://typo3.org)
[![Latest Stable Version](https://img.shields.io/packagist/v/t3graf/extended-bootstrap-package?label=stable&color=33a2d8&logo=packagist&logoColor=white)](https://packagist.org/packages/t3graf/extended-bootstrap-package)
[![Total Downloads](https://img.shields.io/packagist/dt/t3graf/extended-bootstrap-package?color=1081c1&logo=packagist&logoColor=white)](https://packagist.org/packages/t3graf/extended-bootstrap-package)
[![License](https://img.shields.io/packagist/l/t3graf/extended-bootstrap-package?color=498e7f)](https://gitlab.com/typo3graf/developer-team/extensions/extended_bootstrap_package/-/blob/master/LICENSE.md)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/t3graf-extensions/extended_bootstrap_package/main?label=CI%2FCD&logo=gitlab)](https://gitlab.com/t3graf-extensions/extended_bootstrap_package/pipelines)

> Extends Benjamin Kott's Bootstrap package with new, useful functions such as mega menus, an optional top bar panel, new CE's and more. Furthermore, it's a good example how you can adapt the bootstrap package to your own needs.

### Features


* Extra class fields for headline and subheadline
* Top info bar
* Navbar (header) full-width
* Footer full-width
* Footer slide out effect
* Fontawesome 5.14.0 in ckeditor
* Large dropdown menus
* Mega menu
* Supported new cookie consent with EXT: mindshape_cookie_consent,\
  preconfigured for youtube, vimeo
* New Backend Layout for maintenance page
   - with optional backgroundimage slider (see documentation)
* implements the functionality of the EXT:textimage_size
   - thanks to Alessandro & Giulia Tuveri for the code
* 2-col, 3-col, 4-col more flexible grid container with EXT:container
   flexible background-color or image,  width for each column (! experimental)
* EXT: iconpack & bootstrap_package_iconpack
* New CE's
   - Counter group
   - Progress Bar group
   - Countdown
   - Pricing Table
   - Testimonials
   - Team List
   - CTA Buttons
   - Feature List
   - Animated Headline
   - Tile container
   - Tabs container
   - Accordion container
   - Sticky Content
   - Content Animations

* More backend usability

* [Well documented](https://docs.typo3.org/typo3cms/extensions/)

###Features planned

## Installation

### Installation using composer

The recommended way to install the extension is by using [Composer](https://getcomposer.org).

`composer require t3graf/extended-bootstrap-package`.

### Installation from TYPO3 Extension Repository (TER)

Download and install the extension with the extension manager module.

## Minimal setup

1) Include the static TypoScript of the extension. You don't need to include the TypoScript of Bootstrap package.

<!--## 4. Administration

### Create content element

## 5. Configuration

### Extension settings

### Constants
-->
## Contribute

Please create an issue at [gitlab.com/t3graf-extensions/extended_bootstrap_package/issues](https://gitlab.com/t3graf-extensions/extended_bootstrap_package/issues).

**Please use Gitlab only for bug-reports or feature-requests.**

## Credits

This extension was created by Mike Tölle in 2021 for [T3graf media-agentur, Recklinghausen](https://www.t3graf-media.de).

Find examples, use cases and best practices for this extension in our [extended_bootstrap_package blog series on t3graf.de](https://www.t3graf-media.de/blog/).

[Find more TYPO3 extensions we have developed](https://www.t3graf-media.de/) that help us deliver value in client projects. As part of the way we work, we focus on testing and best practices to ensure long-term performance, reliability, and results in all our code.


## Links

- **Demo:** [www.t3graf-media.de/typo3-extensions/extended-bootstrap-package](https://www.t3graf-media.de/typo3-extensions/extended-bootstrap-package)
- **Gitlab Repository:** [gitlab.com/t3graf-extensions/extended_bootstrap_package](https://gitlab.com/t3graf-extensions/extended_bootstrap_package)
- **TYPO3 Extension Repository:** [extensions.typo3.org/](https://extensions.typo3.org/)
- **Found an issue?:** [gitlab.com/t3graf-extensions/extended_bootstrap_package/issues](https://gitlab.com/t3graf-extensions/extended_bootstrap_package/issues)
