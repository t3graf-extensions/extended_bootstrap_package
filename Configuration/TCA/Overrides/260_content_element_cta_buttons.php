<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

// Add Content Element
if (!is_array(@$GLOBALS['TCA']['tt_content']['types']['cta_buttons'])) {
    $GLOBALS['TCA']['tt_content']['types']['cta_buttons'] = [];
}

// Add content element PageTSConfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TsConfig/Page/ContentElement/Element/CtaButtons.tsconfig',
    'Extended Bootstrap Package Content Element: CTA Buttons'
);

// Add content element to selector list
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.cta_buttons',
        'cta_buttons',
        'content-extendedbootstrappackage-cta-buttons',
        'extended',
    ],
    'extended',
    'after'
);

// Assign Icon
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['cta_buttons'] = 'content-extendedbootstrappackage-cta-buttons';

// Configure element type
$GLOBALS['TCA']['tt_content']['types']['cta_buttons'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types']['cta_buttons'],
    [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                tx_extendedbootstrappackage_cta_buttons,
            --div--;LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:cta_buttons.options,
                pi_flexform;LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:advanced,
            --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;language,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                --palette--;;hidden,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                categories,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                rowDescription,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
        ',
    ]
);

// Register fields
$GLOBALS['TCA']['tt_content']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['columns'],
    [
        'tx_extendedbootstrappackage_cta_buttons' => [
            'config' => [
                'appearance' => [
                    'collapseAll' => '1',
                    'enabledControls' => [
                        'dragdrop' => '1',
                    ],
                    'expandSingle' => '0',
                    'levelLinksPosition' => 'both',
                    'showAllLocalizationLink' => '1',
                    'showPossibleLocalizationRecords' => '1',
                    'showRemovedLocalizationRecords' => '1',
                    'showSynchronizationLink' => '0',
                    'useSortable' => '1',
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => '0',
                ],
                'foreign_field' => 'tt_content',
                'foreign_sortby' => 'sorting',
                'foreign_table' => 'tx_extendedbootstrappackage_cta_buttons',
                'minitems' => '1',
                'type' => 'inline',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_db.xlf:tt_content.tx_extendedbootstrappackage_cta_buttons',
        ],
    ]
);

// Add flexForms for content element configuration
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:extended_bootstrap_package/Configuration/FlexForms/CtaButtons.xml',
    'cta_buttons'
);
