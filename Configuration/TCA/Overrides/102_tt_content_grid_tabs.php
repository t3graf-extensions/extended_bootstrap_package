<?php
declare(strict_types = 1);

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') or die('Access denied.');

(static function (): void {
    $gridPartialPaths = [
        'EXT:backend/Resources/Private/Partials/',
        'EXT:container/Resources/Private/Partials/',
        'EXT:extended_bootstrap_package/Resources/Private/Backend/Partials',
    ];

    /**
     * Register grid Tabs
     */
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
        (
        new \B13\Container\Tca\ContainerConfiguration(
            'ce_gridtabs',
            'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.gridTabs.title',
            'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.gridTabs.description',
            [
                [
                    ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.gridTabs.title', 'colPos' => 101],
                ],
            ]
        )
        )
            ->setIcon('EXT:extended_bootstrap_package/Resources/Public/Icons/Container/tabs.svg')
            ->setBackendTemplate('EXT:extended_bootstrap_package/Resources/Private/Backend/Templates/Container/Columns.html')
            ->setGroup('structure')
            ->setGridPartialPaths($gridPartialPaths)
    );
    $GLOBALS['TCA']['tt_content']['types']['ce_gridtabs']['showitem'] = '
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
			--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
			--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
			 --div--;LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tabs.options, pi_flexform;LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:advanced,
		--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
			--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
			--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
			--palette--;;language,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
			--palette--;;hidden,
			--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
			categories,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
			rowDescription,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended
';

    /**
     * Add flexForm
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '*',
        'FILE:EXT:extended_bootstrap_package/Configuration/FlexForms/Container/Tabs.xml',
        'ce_gridtabs'
    );
})();
