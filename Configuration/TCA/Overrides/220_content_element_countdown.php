<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

// Add Content Element
if (!is_array(@$GLOBALS['TCA']['tt_content']['types']['countdown'])) {
    $GLOBALS['TCA']['tt_content']['types']['countdown'] = [];
}

// Add content element PageTSConfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TsConfig/Page/ContentElement/Element/Countdown.tsconfig',
    'Extended Bootstrap Package Content Element: Countdown'
);
// Add content element to selector list
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.countdown',
        'countdown',
        'content-extendedbootstrappackage-countdown',
        'extended'
    ],
    'shortcut',
    'after'
);
// Assign Icon
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['countdown'] = 'content-extendedbootstrappackage-countdown';

// Configure element type
$GLOBALS['TCA']['tt_content']['types']['countdown'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types']['countdown'],
    [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                --palette--;Countdown;countdown,
                --div--;LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:countdown.options,
                pi_flexform;LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:advanced,
            --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;language,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                --palette--;;hidden,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                categories,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                rowDescription,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
        ',
    ]
);
$GLOBALS['TCA']['tt_content']['palettes']['countdown'] = [
    'showitem' => 'countdown_expiry_date',
];
// Register fields
$GLOBALS['TCA']['tt_content']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['columns'],
    [
        'countdown_expiry_date' => [
            'l10n_mode' => 'prefixLangTitle',
            'label' => 'Expiry date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
            ],
        ],
    ]
);
// Add flexForms for content element configuration
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:extended_bootstrap_package/Configuration/FlexForms/Countdown.xml',
    'countdown'
);
