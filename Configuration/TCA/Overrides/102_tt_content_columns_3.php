<?php
declare(strict_types = 1);

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') or die('Access denied.');

(static function (): void {
    $gridPartialPaths = [
        'EXT:backend/Resources/Private/Partials/',
        'EXT:container/Resources/Private/Partials/',
        'EXT:extended_bootstrap_package/Resources/Private/Backend/Partials',
    ];

    /**
     * Register 3 columns
     */
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
        (
        new \B13\Container\Tca\ContainerConfiguration(
            'ce_columns3', // CType
            'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.3Columns.title',
            'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.3Columns.description',
            [
                [
                    ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.col1', 'colPos' => 101],
                    ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.col2', 'colPos' => 102],
                    ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.col3', 'colPos' => 103],
                ],
            ]
        )
        )
            ->setIcon('EXT:container/Resources/Public/Icons/container-3col.svg')
            ->setBackendTemplate('EXT:extended_bootstrap_package/Resources/Private/Backend/Templates/Container/Columns.html')
            ->setGroup('structure')
            ->setGridPartialPaths($gridPartialPaths)
    );
    $GLOBALS['TCA']['tt_content']['types']['ce_columns3']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['ce_columns2']['showitem'];

    /**
     * Add flexForm
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '*',
        'FILE:EXT:extended_bootstrap_package/Configuration/FlexForms/Container/3Columns.xml',
        'ce_columns3'
    );
})();
