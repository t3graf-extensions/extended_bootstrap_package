<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

// Add Content Element
if (!is_array(@$GLOBALS['TCA']['tt_content']['types']['team_members'])) {
    $GLOBALS['TCA']['tt_content']['types']['team_members'] = [];
}

// Add content element PageTSConfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TsConfig/Page/ContentElement/Element/TeamMembers.tsconfig',
    'Extended Bootstrap Package Content Element: Team List'
);

// Add content element to selector list
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.team_list',
        'team_members',
        'content-extendedbootstrappackage-team-list',
        'extended',
    ],
    'shortcut',
    'after'
);
// Assign Icon
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['team_members'] = 'content-extendedbootstrappackage-team-list';

// Configure element type
$GLOBALS['TCA']['tt_content']['types']['team_members'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types']['team_members'],
    [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                tx_extendedbootstrappackage_team_members,
            --div--;LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:advanced,
            --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;language,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                --palette--;;hidden,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                categories,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                rowDescription,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
        ',
    ]
);

// Register fields
$GLOBALS['TCA']['tt_content']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['columns'],
    [
        'tx_extendedbootstrappackage_team_members' => [
            'config' => [
                'appearance' => [
                    'collapseAll' => '1',
                    'enabledControls' => [
                        'dragdrop' => '1',
                    ],
                    'expandSingle' => '0',
                    'levelLinksPosition' => 'top',
                    'showAllLocalizationLink' => '1',
                    'showPossibleLocalizationRecords' => '1',
                    'showRemovedLocalizationRecords' => '1',
                    'showSynchronizationLink' => '0',
                    'useSortable' => '0',
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => '0',
                ],
                'foreign_field' => 'tt_content',
                'foreign_sortby' => 'sorting',
                'foreign_table' => 'tx_extendedbootstrappackage_team_members',
                'type' => 'inline',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.team_members',
        ],
    ]
);
