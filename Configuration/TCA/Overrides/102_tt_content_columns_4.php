<?php
declare(strict_types = 1);

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') or die('Access denied.');

(static function (): void {
    $gridPartialPaths = [
        'EXT:backend/Resources/Private/Partials/',
        'EXT:container/Resources/Private/Partials/',
        'EXT:extended_bootstrap_package/Resources/Private/Backend/Partials',
    ];

    /**
     * Register 4 columns
     */
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
        (
        new \B13\Container\Tca\ContainerConfiguration(
            'ce_columns4',
            'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.4Columns.title',
            'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.4Columns.description',
            [
                [
                    ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.col1', 'colPos' => 101],
                    ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.col2', 'colPos' => 102],
                    ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.col3', 'colPos' => 103],
                    ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tx_container.col4', 'colPos' => 104],
                ],
            ]
        )
        )
            ->setIcon('EXT:container/Resources/Public/Icons/container-4col.svg')
            ->setBackendTemplate('EXT:extended_bootstrap_package/Resources/Private/Backend/Templates/Container/Columns.html')
            ->setGroup('structure')
            ->setGridPartialPaths($gridPartialPaths)
    );
    $GLOBALS['TCA']['tt_content']['types']['ce_columns4']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['ce_columns2']['showitem'];

    /**
     * Add flexForm
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '*',
        'FILE:EXT:extended_bootstrap_package/Configuration/FlexForms/Container/4Columns.xml',
        'ce_columns4'
    );
})();
