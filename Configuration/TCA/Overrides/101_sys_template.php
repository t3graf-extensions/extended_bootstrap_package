<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('extended_bootstrap_package', 'Configuration/TypoScript', 'Extended Bootstrap Package');
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('extended_bootstrap_package', 'Configuration/TypoScript/Extensions/maps2/OSMaps', 'Extended Bootstrap Package: Map2 for OpenStreetMap');
