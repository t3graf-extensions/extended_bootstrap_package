<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

$animationColumns = [
    'animation' => [
        'exclude' => true,
        'label' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:animation_style',
        'config' => [
            'items' => [
                ['LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:item.no-animation', ''],
                ['LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:item.fade-animations', '--div--'],
                ['fade', 'fadeIn', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/fade.gif'],
                ['fade-up', 'fadeInUp', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/fade-up.gif'],
                ['fade-down', 'fadeInDown', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/fade-down.gif'],
                ['fade-right', 'fadeInRight', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/fade-right.gif'],
                ['fade-left', 'fadeInLeft', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/fade-left.gif'],
                ['fade-up-right', 'fadeInBottomLeft', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/fade-up-right.gif'],
                ['fade-up-left', 'fadeInBottomRight', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/fade-up-left.gif'],
                ['fade-down-right', 'fadeInTopLeft', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/fade-down-right.gif'],
                ['fade-down-left', 'fadeInTopRight', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/fade-down-left.gif'],
                ['LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:item.flip-animations', '--div--'],
                ['flip-up', 'flipOutX', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/flip-up.gif'],
                ['flip-down', 'flipInX', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/flip-down.gif'],
                ['flip-left', 'flipInY', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/flip-left.gif'],
                ['flip-right', 'flipOutY', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/flip-right.gif'],
                ['LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:item.slide-animations', '--div--'],
                ['slide-up', 'slideInUp', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/slide-up.gif'],
                ['slide-down', 'slideInDown', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/slide-down.gif'],
                ['slide-right', 'slideInLeft', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/slide-right.gif'],
                ['slide-left', 'slideInRight', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/slide-left.gif'],
                ['LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:item.zoom-animations', '--div--'],
                ['zoom-in', 'zoomIn', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/zoom-in.gif'],
                ['zoom-in-up', 'zoomInUp', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/zoom-in-up.gif'],
                ['zoom-in-down', 'zoomInDown', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/zoom-in-down.gif'],
                ['zoom-in-right', 'zoomInLeft', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/zoom-in-right.gif'],
                ['zoom-in-left', 'zoomInRight', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/zoom-in-left.gif'],
                ['zoom-out', 'zoomOut', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/zoom-out.gif'],
                ['zoom-out-up', 'zoomOutUp', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/zoom-out-up.gif'],
                ['zoom-out-down', 'zoomOutDown', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/zoom-out-down.gif'],
                ['zoom-out-right', 'zoomOutLeft', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/zoom-out-right.gif'],
                ['zoom-out-left', 'zoomOutRight', 'EXT:extended_bootstrap_package/Resources/Public/Icons/Animations/zoom-out-left.gif'],
            ],
            'renderType' => 'selectSingle',
            'type' => 'select',
            'size' => 1,
            'default' => '',
        ],
    ],
    'animation_duration' => [
        'exclude' => true,
        'label' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:label.animation_duration',
        'description' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:description.animation_duration',
        'config' => [
            'type' => 'input',
            'size' => 5,
            'eval' => 'trim,int',
            'range' => [
                'lower' => 400,
                'upper' => 3000,
            ],
            'default' => 1500,
            'slider' => [
                'step' => 50,
                'width' => 200,
            ],
        ],
    ],
    'animation_delay' => [
        'exclude' => true,
        'label' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:label.animation_delay',
        'description' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:description.animation_delay',
        'config' => [
            'type' => 'input',
            'size' => 5,
            'eval' => 'trim',
            'range' => [
                'lower' => 0,
                'upper' => 3000,
            ],
            'default' => 0,
            'slider' => [
                'step' => 50,
                'width' => 200,
            ],
        ],
    ],
    'animation_iteration' => [
        'exclude' => true,
        'label' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:label.animation_iteration',
        'description' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:description.animation_iteration',
        'config' => [
            'type' => 'input',
            'size' => 5,
            'eval' => 'trim,int',
            'range' => [
                'lower' => 0,
                'upper' => 3000,
            ],
            'default' => 1,
            'slider' => [
                'step' => 50,
                'width' => 200,
            ],
        ],
    ],
];

$GLOBALS['TCA']['tx_bootstrappackage_icon_group_item']['palettes']['animation'] = [
    'label' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:palette_animation',
    'showitem' => 'animation, --linebreak--, animation_duration, animation_delay, animation_iteration',
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_bootstrappackage_icon_group_item', $animationColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tx_bootstrappackage_icon_group_item',
    '--div--;LLL:EXT:extended_bootstrap_package/Resources/Private/Language/Backend.xlf:tab_animation,--palette--;;animation',
    '',
    ''
);
