<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') or die('Access denied.');

(static function (): void {
    /**
     * @todo Might be removed upon solving b13/container issue 272
     */
    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('container')) {
        foreach ($GLOBALS['TCA']['tt_content']['containerConfiguration'] as &$config) {
            if (!isset(array_flip($config['gridPartialPaths'])['EXT:extended_bootstrap_package/Resources/Private/Backend/Partials'])) {
                $config['gridPartialPaths'][] = 'EXT:extended_bootstrap_package/Resources/Private/Backend/Partials';
            }
        }
        unset($config);
    }
})();
